#!/usr/bin/make -f

UPSTREAM_GIT ?= https://opendev.org/openstack/cinder.git
include /usr/share/openstack-pkg-tools/pkgos.make

%:
	dh $@ --buildsystem=pybuild --with python3,sphinxdoc

override_dh_auto_clean:
	rm -f debian/cinder-common.config debian/cinder-common.postinst debian/cinder-api.config debian/cinder-api.postinst debian/cinder-volume.postinst debian/cinder-common.postrm
	rm -rf debian/*.upstart debian/*.service debian/*.templates
	if [ -e debian/po-backup ] ; then \
		rm -rf debian/po ; \
		mv debian/po-backup debian/po ; \
	fi
	rm -rf build $(CURDIR)/debian/*.init
	find . -name '*.pyc' -delete
	rm -rf .stestr
	rm -rf cinder.egg-info
	rm -f AUTHORS ChangeLog
	rm -rf doc/source/contributor/api doc/source/drivers.rst
	rm -f doc/source/_static/cinder.conf.sample doc/source/_static/cinder.policy.yaml.sample
	rm -rf .datacore_cha conversion
	rm -f .datacore_chap
	find . -iname '*.pyc' -delete
	for i in $$(find . -type d -iname __pycache__) ; do rm -rf $$i ; done

override_dh_auto_install:
	echo "Do nothing..."

override_dh_auto_test:
	echo "Do nothing..."

override_dh_auto_build:
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func cinder-common.config
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func cinder-common.postinst
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func cinder-api.config
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func cinder-api.postinst
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_func cinder-volume.postinst
	/usr/share/openstack-pkg-tools/pkgos_insert_include pkgos_postrm cinder-common.postrm
	cp -auxf debian/po debian/po-backup
	pkgos-merge-templates cinder-api cinder endpoint
	pkgos-merge-templates cinder-common cinder db rabbit ksat

override_dh_install:
	python3 setup.py install --install-layout=deb --root $(CURDIR)/debian/tmp
	rm -rf $(CURDIR)/debian/tmp/usr/etc

ifeq (,$(findstring nocheck, $(DEB_BUILD_OPTIONS)))
	# See https://bugs.launchpad.net/cinder/+bug/2000436
	# See https://bugs.launchpad.net/cinder/+bug/2046227
	# cinder.tests.unit.objects.test_objects.TestObjectVersions.test_versions : object hash changed
	pkgos-dh_auto_test --no-py2 'cinder\.tests\.unit\.(?!(volume\.drivers\.test_nfs.*)|volume\.drivers\.test_rbd\.RBDTestCase\.test_delete_volume_not_found|.*backup\.drivers\.test_backup_ceph\.BackupCephTestCase.*|.*volume\.drivers\.hpe\.test_hpe3par.*|.*objects\.test_objects\.TestObjectVersions\.test_versions.*|test_cmd\.TestCinderBackupCmd\.test_main_multiprocess)'
endif

	mkdir -p $(CURDIR)/debian/cinder-common/usr/share/cinder-common
	PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages oslo-config-generator \
		--output-file $(CURDIR)/debian/cinder-common/usr/share/cinder-common/cinder.conf \
		--wrap-width 140 \
		--namespace castellan.config \
		--namespace cinder \
		--namespace keystonemiddleware.auth_token \
		--namespace osprofiler \
		--namespace oslo.config \
		--namespace oslo.concurrency \
		--namespace oslo.db \
		--namespace oslo.log \
		--namespace oslo.messaging \
		--namespace oslo.middleware \
		--namespace oslo.policy \
		--namespace oslo.privsep \
		--namespace oslo.reports \
		--namespace oslo.service.periodic_task \
		--namespace oslo.service.service \
		--namespace oslo.service.sslutils \
		--namespace oslo.service.wsgi \
		--namespace oslo.versionedobjects \
		--namespace os_brick

	pkgos-readd-keystone-authtoken-missing-options $(CURDIR)/debian/cinder-common/usr/share/cinder-common/cinder.conf keystone_authtoken cinder

	mkdir -p $(CURDIR)/debian/cinder-common/etc/policy.d
	PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages oslopolicy-sample-generator \
		--output-file $(CURDIR)/debian/cinder-common/etc/policy.d/00_default_policy.yaml \
		--format yaml \
		--namespace cinder

	# Comment out deprecated policies
	sed -i 's/^"/#"/' $(CURDIR)/debian/cinder-common/etc/policy.d/00_default_policy.yaml


	# Use the policy.d folder
	pkgos-fix-config-default $(CURDIR)/debian/cinder-common/usr/share/cinder-common/cinder.conf oslo_policy policy_dirs /etc/cinder/policy.d

	dh_install
	dh_missing --fail-missing -Xbin/cinder-all
	install -D -m 0440 debian/cinder-common.sudoers $(CURDIR)/debian/cinder-common/etc/sudoers.d/cinder-common

	pkgos-fix-config-default $(CURDIR)/debian/cinder-common/usr/share/cinder-common/cinder.conf DEFAULT auth_strategy keystone

	# Set LVM as default backend
	pkgos-fix-config-default $(CURDIR)/debian/cinder-common/usr/share/cinder-common/cinder.conf DEFAULT enabled_backends lvm
	echo "[lvm]" 				>> $(CURDIR)/debian/cinder-common/usr/share/cinder-common/cinder.conf
	echo "volume_driver = cinder.volume.drivers.lvm.LVMVolumeDriver" >> $(CURDIR)/debian/cinder-common/usr/share/cinder-common/cinder.conf
	echo "volume_group = pkgosvg0" 		>> $(CURDIR)/debian/cinder-common/usr/share/cinder-common/cinder.conf
	echo "iscsi_protocol = iscsi" 		>> $(CURDIR)/debian/cinder-common/usr/share/cinder-common/cinder.conf
	echo "iscsi_helper = tgtadm" 		>> $(CURDIR)/debian/cinder-common/usr/share/cinder-common/cinder.conf

	# Set a default value for lock_path & state_path
	pkgos-fix-config-default $(CURDIR)/debian/cinder-common/usr/share/cinder-common/cinder.conf oslo_concurrency lock_path /var/lock/cinder

	# Fix the missing sql_connection directive
	pkgos-fix-config-default $(CURDIR)/debian/cinder-common/usr/share/cinder-common/cinder.conf database sqlite:////var/lib/cinder/cinderdb

	install -D -m 0644 $(CURDIR)/etc/cinder/logging_sample.conf $(CURDIR)/debian/cinder-common/usr/share/cinder-common/logging.conf

override_dh_fixperms:
	dh_fixperms -Xcinder_tgt.conf
	chmod 0440 $(CURDIR)/debian/cinder-common/etc/sudoers.d/cinder-common

override_dh_sphinxdoc:
ifeq (,$(findstring nodoc, $(DEB_BUILD_OPTIONS)))
	PYTHONPATH=$(CURDIR)/debian/python3-cinder/usr/lib/python3/dist-packages PYTHON=python3 python3 -m sphinx -b html doc/source $(CURDIR)/debian/cinder-doc/usr/share/doc/cinder-doc/html
	dh_sphinxdoc
endif

override_dh_python3:
	dh_python3 --shebang=/usr/bin/python3
